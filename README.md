README.md 20150616 - 20130304

This repo is dedicated to building FlightGear
 http://flightgear.org

Each folder should have a README.txt, which describes what can 
be found in that folder...

At present there are two primary 'flavors'...

Folder  Description

linux - A 'makefg' script to download and build 
        fgfs, in Ubuntu, trying to take care of all the 
        dependencies.
        
        All that should be requred is that it be copied 
        to either a PATH in our environment PATH variable,
        or directly to the root folder wher you wish to do the build.

        Run with $ makefg -? it will show the considerable number 
        of options it accepts.

        One important one is NO_TOOL_UPD, to ask it to NOT try 
        to install any 'packages', as these are very distro specific.

windows - A developing set of batch files, at present 
        a little incomplete and quite messy. It requires LOTS 
        of special SETUP to function. It is not really present here 
        for your usage directly, more one idea on how to arrange 
        all this.

        In the main it follows the directory tree given in this wiki
        http://wiki.flightgear.org/Building_using_CMake_-_Windows

        In general the batch files presented can be run by 'jenkins',
        with the particular windows box connected to that server as 
        a remote node, with one single command, like WIN Simgear 
        would be 'build-sg'.


General:

Recently Pat presented a good 'build strategy', as follows.
Certainly, these building tools try to take most of these 
items into account, to varying degrees...


One of several Goals:
 
    I want to build an application
         - from sources
         - of any chosen branch or version,
         - with a choice of one or more sets of options
         - with pre-requisite libaries installed as needed
         - with the sources set up and ready to use in my choice of IDE
         - and with build logs to check when the build fails
 

Execution Control:
 
        - Concurrent or sequential runs with different options or
           targets, started separately from the command line, or
           together using multiple configuration files
 
        - load option values from command line or one or
           more configuration files
 
        - Separately Log build runs for each configuration
 
Information Needs:
 
        - Pre-requisites with a way of targeting a pre-requisite
           version to an application component version or branch
         - Application build order (dependencies)
         - previously obtained source code
         - trees to search for resources
         - preferred locations for resources
         - how to set up a project in one or more specific IDEs
 
Prepare Task List:
 
        - Determine option values for builds
                 - features and library options
                 - build target options (build targets include distros)
 
        - Check that the required prerequisite libraries and their
           versions are available
 
        - Provide a list of prerequisite libraries that do not have
           the required version
 
        - Determine what prerequisite libraries and
           application components need to be built or installed.
 
        - Prepare task list - can be organized by type of task or by
           target.  task order will need to be considered somewhere.
 
Tasks:
         - install any prerequisites as needed
 
        - Get or update sources that need to be built, managing the use
           of local and network resources appropriately
 
        - Set sources to the correct branch or version
 
        - Optionally integrate or synchronize sources with an IDE
 
        - Optionally build patches from a list of supplied working
           directories
 
        - Optionally apply patches
 
        - Use or provide a build directory
 
        - Use or provide an install directory
 
        - build
 
        - install
 
        - check build results
 
        - Optionally rerun any failed builds to include
           re-configuration if it wasn't already done.
 
        - export build result and artifacts
 
        - run automatic tests


; eof
