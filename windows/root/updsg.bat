@setlocal
@set TEMPD=simgear
@set TMPREPO=git://gitorious.org/fg/simgear.git
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" @set DOPAUSE=echo NO PAUSE requested

@if EXIST %TEMPD%\nul goto UPDATE

@echo Are you SURE continue to do CLONE of SG source?
@echo Will do: call git clone %TMPREPO% %TEMPD%

@%DOPAUSE%

call git clone %TMPREPO% %TEMPD%
@echo Done: call git clone %TMPREPO% %TEMPD%
@if NOT EXIST %TEMPD%\. goto COFAILED

@cd %TEMPD%
@call git branch
@cd ..

@REM === seems not required ===
@REM echo Done new checkout of source... changing to 'next'???
@REM call git clone git://gitorious.org/fg/simgear.git %TEMPD%
@REM call git branch -t -l next origin/next
@REM call git checkout next

@echo Done new clone...
@goto END

:UPDATE
@echo This is an UPDATE of simgear
@cd %TEMPD%
@call git status
@echo *** CONTINUE? ***
@%DOPAUSE%

@echo In %CD% doing: 'call git pull origin next'
@call git pull
@echo Done: 'call git pull origin next'
@cd ..
@goto END

:COFAILED
@echo ERROR: %TEMPD% not created - clone FAILED
@endlocal
@exit /b 1

:END
@endlocal

@REM eof
