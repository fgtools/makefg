README.md - 20150616 - 20130304

The batch files are to download, clone, checkout the 
primary source.

The are intended to be in some root folder, like say C:\FG\17,
and can be used to commence to build a build tree, much like 
that described in this wiki -

 http://wiki.flightgear.org/Building_using_CMake_-_Windows

In that wiki the 'root' folder is referred to as ${MSVC_3RDPARTY_ROOT}






; eof
