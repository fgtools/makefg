@echo Update FG next...
@echo http://gitorious.org/fg
@setlocal
@set REPO=git://gitorious.org/fg/flightgear.git
@set TEMPD=flightgear
@set DOPAUSE=pause
@if "%~1x" == "NOPAUSEx" @set DOPAUSE=echo NO PAUSE requested

@if NOT EXIST %TEMPD%\. goto CHECKOUT

@echo Doing update
@cd %TEMPD%
@call git status
@echo *** CONTINUE? ***
@%DOPAUSE%

git pull origin next
@cd ..

@goto END

:CHECKOUT
@echo WARNING: Folder %TEMPD% does NOT exist!
@echo This is a NEW CLONE
@echo Will do: 'git clone %REPO% %TEMPD%'
@echo *** CONTINUE? *** Only Ctrl+c to abort... all other keys continue...
@%DOPAUSE%

@echo Doing: 'git clone %REPO% %TEMPD%'

git clone %REPO% %TEMPD%

@if NOT EXIST %TEMPD%\nul goto FAILED
@echo Done fresh checkout...

@goto END

:FAILED
@echo ERROR: git clone FAILED! No folder %TEMPD% created...
@goto END

:END
@endlocal
