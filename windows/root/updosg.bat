@setlocal
@echo 2012-05-15 - 2012-02-04 - Update OSG TRUNK
@set REPO=http://www.openscenegraph.org/svn/osg/OpenSceneGraph/trunk
@REM svn checkout http://www.openscenegraph.org/svn/osg/OpenSceneGraph/trunk
@REM 2011-10-14 - Updated to revision 12829.
@REM 2010-04-19: from : http://www.openscenegraph.org/projects/osg/wiki/Downloads/SVN
@REM svn checkout http://www.openscenegraph.org/svn/osg/OpenSceneGraph/trunk OpenSceneGraph
@REM Fresh checkout - 2008/07/12 - svn:8570
@REM From : http://www.openscenegraph.org/projects/osg/wiki/Downloads/SVN
@REM svn checkout http://www.openscenegraph.org/svn/osg/OpenSceneGraph/trunk OpenSceneGraph
@REM Fresh checkout - 2008/04/26
@REM svn checkout http://www.openscenegraph.org/svn/osg/OpenSceneGraph/trunk OpenSceneGraph
@REM Noted 2007/03/18 OpenSceneGraph and OpenThreads now using SVN ...
@REM svn checkout http://www.openscenegraph.com/svn/osg/OpenThreads/trunk OpenThreads
@REM svn checkout http://www.openscenegraph.com/svn/osg/OpenSceneGraph/trunk OpenSceneGraph
@REM Producer, still under CSV, moved out of core of OSG
@REM svn checkout http://www.openscenegraph.com/svn/Producer/osgProducer/trunk osgProducer
@REM and Producer remains ...
@REM press return for password 
@REM cvs -d :pserver:anonymous@andesengineering.com:/cvs/Producer login 
@REM cvs -d :pserver:anonymous@andesengineering.com:/cvs/Producer co Producer 
@REM OSG CVS version is now in OSGcvs, and OpenThreads in OTcvs
@set TEMPUP=OSGtrunk
@REM set TEMPUP=openscenegraph

@if NOT EXIST %TEMPUP%\. goto NEWCO
@echo Doing an UPDATE in %TEMPUP%...

cd %TEMPUP%
svn up
cd ..

@goto END

:NEWCO
@echo This looks like a NEW checkout... Folder %TEMPUP% does NOT exist
@echo Will do 'svn checkout %REPO% %TEMPUP%'
@echo *** CONTINUE? *** Only Ctrl+c will abort! All other keys continue..
@pause
@echo Doing 'svn checkout %REPO% %TEMPUP%'
svn checkout %REPO% %TEMPUP%
@if NOT EXIST %TEMPUP%\. goto NOCO
@echo Done...
@goto END

:NOCO
@echo ERROR: Folder %TEMPUP% NOT created! svn ERROR!
@goto END

:END
@endlocal
