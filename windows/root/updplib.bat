@setlocal
@REM http://sourceforge.net/projects/plib/

@set TMPSRC=plib

@if NOT EXIST %TMPSRC%\nul goto DOCO
cd %TMPSRC%
svn update
cd ..
@goto END

:DOCO
@echo.
@echo This is NEW checkout inpto %TMPSRC%
@set TMPREPO=https://plib.svn.sourceforge.net/svnroot/plib/trunk
@echo Will do: svn co %TMPREPO% %TMPSRCS%
@echo *** CONTINUE? *** Only Ctrl+c aborts. All other keys continue...
@pause

svn co %TMPREPO% %TMPSRC%

@if NOT EXIST %TMPSRC%\nul goto NOCO

@echo DOne fresh checkout to to %TMPSRC%

@goto END

:NOCO
@echo ERROR: Appears SVN checkout FAILED! No %TMPSRC%
@goto END



:END
@endlocal

