file : README.txt - 20130304

The batch files are to build the various primary sources.

The are intended to be in some bin folder, like say C:\MDOS,
which is IN the PATH environment variable, so that they can 
be called from anywhere.

Each use the hfg-current.bat to establish the 'root' directory
of the tree, much like that described in this wiki -

 http://wiki.flightgear.org/Building_using_CMake_-_Windows

In that wiki the 'root' folder is referred to as ${MSVC_3RDPARTY_ROOT}

They output a build log to the folder
 C:\Projects\workspace\%TMPJOB%
where the TMPJOB is set to win-<short-project-name>

So for SimGear this will be
 C:\Projects\workspace\win-sg
and the log files will be consectutively named bldlog-1.txt, 
bldlog-2.txt, etc.

While the final 'win-sg' folder will be created if it does not 
the exist, the primary folder, C:\Projects\workspace, MUST EXIST. 



# eof
