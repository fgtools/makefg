@setlocal
@set TMPBGN=%TIME%

@set ADDUPD=0
@set ADDINST=1
@set ADDDBG=1
@set ADDCLEAN=0
@set ADDDEBUG=
@REM set ADDDEBUG=-v9 -d extra -ll

@REM Get to the right FOLDER
@call hfg-current

@set TMPPROJ=OSGtrunk
@set TMPVERS=
@set TMPJENK=win-osg
@set TMPMSVC=msvc100

@set TMPBASE=%CD%
@set TMPBLD=%TMPBASE%\build-%TMPPROJ%
@set TMPSRC=%TMPBASE%\%TMPPROJ%%TMPVERS%
@set TMPGEN=Visual Studio 10
@set TMPINST=%TMPBASE%\install\%TMPMSVC%\OpenSceneGraph
@set TMP3RD=%TMPBASE%\3rdParty
@set TMPIFIL=%TMP3RD\installed.txt
@set TMPCZIP=%TMPPROJ%-cmake.zip
@set TMPOSG=%TMPBASE%\install\%TMPMSVC%\OpenSceneGraph
@set TMPBOOST=%TMPBASE%\boost-trunk

@REM Setup for the BUILD
@set TMPJOB=%JOB_NAME%
@set TMPDST=%WORKSPACE%
@set TMPVER=%BUILD_NUMBER%

@REM Not used
@set TMPIPATH=%TMPINST%\include
@set TMPLPATH=%TMPINST%\lib

@REM Set OPTIONS, if any
@set TMPOPTS=
@REM set TMPOPTS=%TMPOPTS% -DCMAKE_DEBUG_POSTFIX=d
@REM set TMPOPTS=%TMPOPTS% -DOPENAL_LIB_DIR:PATH=%TMPINST%\lib
@REM set TMPOPTS=%TMPOPTS% -DOPENAL_INCLUDE_DIR:PATH=%TMPINST%\include

@if "%TMPJOB%x" == "x" (
@set TMPJOB=%TMPJENK%
)

@if "%TMPDST%x" == "x" (
@set TMPDST=C:\Projects\workspace\%TMPJOB%
)

@if NOT EXIST %TMPDST%\nul (
@md %TMPDST%
@if ERRORLEVEL 1 goto ISERR
)

@REM Set the VERSION LOG file
@if "%TMPVER%x" == "x" goto GETVER
@set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@goto GOTVER
:GETVER
@set TMPVER=0
:RPT
@set /A TMPVER+=1
@set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@if EXIST %TMPLOG% goto RPT
:GOTVER
@set TMPCZIP=%TMPPROJ%-cmake-%TMPVER%.zip

@echo Bgn %DATE% %TIME% to %TMPLOG%
@REM Restart LOG
@echo. > %TMPLOG%
@echo Bgn %DATE% %TIME% >> %TMPLOG%
@REM if ERRORLEVEL 1 goto ISERR
@echo NUM 1: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@echo Begin in folder %CD% >> %TMPLOG%

@REM get/update source
@if "%ADDUPD%x" == "1x" (
@call updosg NOPAUSE  >> %TMPLOG% 2>&1
@echo Done %TMPPROJ% source update >> %TMPLOG%
)

@REM Check we are in the right place, and can FIND the SOURCE
@if NOT EXIST %TMPSRC%\nul goto NOSRC
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOSRC2

@if NOT EXIST %TMPBLD%\nul (
@md %TMPBLD%
)
@if NOT EXIST %TMPBLD%\nul goto NODIR

@cd %TMPBLD%
@if ERRORLEVEL 1 goto ISERR
@echo Building in folder %CD% >> %TMPLOG%

@echo Deal with the CMakeLists.txt files >> %TMPLOG%
@echo Is native CMake so nothing to do here >> %TMPLOG%
@if EXIST build-cmake.bat (
@echo Generate cmake list files >> %TMPLOG%
@call build-cmake >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ISERR
) else (
@echo build-cmake.bat NOT found >> %TMPLOG%
)

@REM Check for primary CMakeLists.txt file
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOSRC2

@REM All looks ok to proceed with BUILD

@echo Establish MSVC + SDK environment >> %TMPLOG%
@call set-msvc-sdk >> %TMPLOG%
@echo NUM 2: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@REM Set ENVIRONMENT, if any

@REM Do cmake configure and generation
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH=%TMP3RD%
@REM set TMPOPTS=%TMPOPTS% -DCMAKE_LIBRARY_PATH=%TMPLPATH%
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINST%

@REM Do NOT need CMAKE_PROGRAM_PATH I think... but maybe later for say fluid.exe
@echo Doing: cmake %TMPSRC% -G "%TMPGEN%"  %TMPOPTS% >> %TMPLOG%
cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto NOCM

@echo Do cmake Release build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Release' >> %TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@echo Done: 'cmake --build . --config Release' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLD

@echo Install and generate the Release ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Release' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Release >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Release ZIP created! >> %TMPLOG%
)

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake -DBUILD_TYPE=Release -P cmake_install.cmake' >> %TMPLOG%
cmake -DBUILD_TYPE=Release -P cmake_install.cmake >> %TMPLOG%  2>&1
@echo Done: 'cmake -DBUILD_TYPE=Release -P cmake_install.cmake' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Release install %DATE% %TIME% >> %TMPIFIL%
@echo Installed to %TMPINST% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt install-%TMPPROJ%-rel.txt >nul
)
)

@if NOT "%ADDDBG%x" == "1x" (
@echo NOTE: Debug build is DISABLED! >> %TMPLOG%
@goto DONE
)

@echo Do cmake Debug build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Debug' >> %TMPLOG%
cmake --build . --config Debug >> %TMPLOG%
@echo Done: 'cmake --build . --config Debug' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLD

@echo Install and generate the Debug ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Debug' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Debug >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Debug ZIP created! >> %TMPLOG%
)

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake -DBUILD_TYPE=Debug -P cmake_install.cmake' >> %TMPLOG%
cmake -DBUILD_TYPE=Debug -P cmake_install.cmake >> %TMPLOG%
@echo Done: 'cmake -DBUILD_TYPE=Debug -P cmake_install.cmake' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Debug install %DATE% %TIME% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt install-%TMPPROJ%-dbg.txt >nul
)
)

:DONE

@echo Looks successful - keep the CMakeLists.txt, in '%TMPCZIP%' >> %TMPLOG%
@if EXIST %TMPCZIP% del %TMPCZIP% >nul
@echo Doing: 'call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt' >> %TMPLOG%
@call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt >nul 2>&1
@if EXIST %TMPCZIP% (
copy %TMPCZIP% %TMPDST% >nul
@call unzip8 -vb %TMPCZIP% >> %TMPLOG%
) else (
@echo WARNING: Appears ZIP of %TMPCZIP% failed, but considered non-critical! >> %TMPLOG%
)

@if NOT "%ADDINST%x" == "1x" (
@echo NOTE: Install to %TMPINST% is DISABLED >> %TMPLOG%
)
@call elapsed %TMPBGN% >> %TMPLOG%
@echo End %DATE% %TIME% ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@call elapsed %TMPBGN%
@echo End %DATE% %TIME% Success
@goto END

:ZIPERR
@echo ERROR: build ZIP failed! returning ERRORLEVEL=%ERRORLEVEL%
@goto ISERR

:NOINST
@echo ERROR: CMake install error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOBLD
@echo ERROR: CMake build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOCM
@echo ERROR: CMake configure and generation error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOSRC
@echo ERROR: Could NOT locate SOURCE! %TMPSRC% >> %TMPLOG%
@goto ISERR

:NOSRC2
@echo ERROR: Could NOT locate SOURCE! %TMPSRC%\CMakeLists.txt >> %TMPLOG%
@goto ISERR

:NODIR
@echo ERROR: Could NOT create build directory %TMPBLD%! >> %TMPLOG%
@goto ISERR

:ISERR
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%!
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
