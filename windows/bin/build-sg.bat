@setlocal
@set TMPBGN=%TIME%

@set ADDUPD=0
@set ADDINST=1
@set ADDDBG=1
@set ADDCLEAN=0
@set ADDDEBUG=
@REM set ADDDEBUG=-v9 -d extra

@REM Get to the right FOLDER
@call hfg-current

@set TMPPROJ=simgear
@set TMPPRJ=sg
@set TMPVERS=
@set TMPJENK=win-%TMPPRJ%
@set TMPMSVC=msvc100

@set TMPBASE=%CD%
@set TMPBLD=%TMPBASE%\build-%TMPPRJ%
@set TMPSRC=%TMPBASE%\%TMPPROJ%%TMPVERS%
@set TMPGEN=Visual Studio 10
@set TMPINST=%TMPBASE%\install\msvc100\%TMPPROJ%
@set TMPIFIL=%TMPBASE%\3rdParty\installed.txt
@set TMPOSG=%TMPBASE%\install\%TMPMSVC%\openscenegraph
@set TMPBOOST=%TMPBASE%\boost_1_44_0

@REM Setup for the BUILD
@set TMPJOB=%JOB_NAME%
@set TMPDST=%WORKSPACE%
@set TMPVER=%BUILD_NUMBER%
@set TMP3RD=%TMPBASE%\3rdParty
@set TMPIPATH=%TMP3RD%\include
@set TMPLPATH=%TMP3RD%\lib
@set TMPOPTS=
@set TMPOPTS=%TMPOPTS% -DMSVC_3RDPARTY_ROOT=%TMPBASE%
@set TMPOPTS=%TMPOPTS% -DENABLE_LIBSVN:BOOL=OFF

@if "%TMPJOB%x" == "x" (
@set TMPJOB=%TMPJENK%
)

@if "%TMPDST%x" == "x" (
@set TMPDST=C:\Projects\workspace\%TMPJOB%
)

@if NOT EXIST %TMPDST%\nul (
@md %TMPDST%
@if ERRORLEVEL 1 goto ISERR
)

@REM Set the VERSION LOG file
@if "%TMPVER%x" == "x" goto GETVER
@set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@goto GOTVER
:GETVER
@set TMPVER=0
:RPT
@set /A TMPVER+=1
@set TMPLOG=%TMPDST%\bldlog-%TMPVER%.txt
@if EXIST %TMPLOG% goto RPT
:GOTVER
@set TMPCZIP=%TMPDST%\%TMPPROJ%-cmake-%TMPVER%.zip

@echo Bgn %DATE% %TIME% to %TMPLOG%
@REM Restart LOG
@echo. > %TMPLOG%
@echo Bgn %DATE% %TIME% >> %TMPLOG%
@REM if ERRORLEVEL 1 goto ISERR
@echo NUM 1: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@call show-sg >> %TMPLOG% 2>&1

@echo Begin in folder %CD% >> %TMPLOG%

@REM get/update source
@if "%ADDUPD%x" == "1x" (
@call upd%TMPPRJ% NOPAUSE  >> %TMPLOG% 2>&1
@echo Done %TMPPROJ% source update >> %TMPLOG%
)

@REM Check we are in the right place, and can FIND the SOURCE
@if NOT EXIST %TMPSRC%\nul goto NOSRC

@if NOT EXIST %TMPBLD%\nul (
@md %TMPBLD%
)
@if NOT EXIST %TMPBLD%\nul goto NODIR

@cd %TMPBLD%
@if ERRORLEVEL 1 goto ISERR

@echo Building in folder %CD% >> %TMPLOG%

@echo Deal with the CMakeLists.txt files >> %TMPLOG%
@echo Is native CMake so nothing to do here >> %TMPLOG%
@if EXIST build-cmake.bat (
@echo Generate cmake list files >> %TMPLOG%
@call build-cmake >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ISERR
) else (
@echo build-cmake.bat not found in %CD% >> %TMPLOG%
)

@REM Check for primary CMakeLists.txt file

@if NOT EXIST %TMPSRC%\CMakeLists.txt goto NOSRC2

@REM All looks ok to proceed with BUILD

@echo Establish MSVC + SDK environment >> %TMPLOG%
@call set-msvc-sdk >> %TMPLOG%
@echo NUM 2: ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%

@echo Set more ENVIRONMENT, if any >> %TMPLOG%
@set OSG_DIR=%TMPOSG%
@echo Set ENVIRONMENT OSG_DIR=%OSG_DIR% >> %TMPLOG%
@set BOOST_INCLUDEDIR=%TMPBOOST%
@echo Set ENVIRONMENT BOOST_INCLUDEDIR=%BOOST_INCLUDEDIR% >> %TMPLOG%

@echo Do cmake configure and generation >> %TMPLOG%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH=%TMPIPATH%
@set TMPOPTS=%TMPOPTS% -DCMAKE_LIBRARY_PATH=%TMPLPATH%
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINST%

@echo Doing: cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS% >> %TMPLOG%
@cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS% >> %TMPLOG% 2>&1
@echo Done: cmake %TMPSRC% -G "%TMPGEN%" %TMPOPTS% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOCM

@echo Do cmake Release build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Release' >> %TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@echo Done: 'cmake --build . --config Release' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLD

@echo Install and generate the Release ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Release' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Release >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Release ZIP created! >> %TMPLOG%
)

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake -DBUILD_TYPE=Release -P cmake_install.cmake' >> %TMPLOG%
cmake -DBUILD_TYPE=Release -P cmake_install.cmake >> %TMPLOG%  2>&1
@echo Done: 'cmake -DBUILD_TYPE=Release -P cmake_install.cmake' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Release install %DATE% %TIME% >> %TMPIFIL%
@echo Installed to %TMPINST% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt install-%TMPPROJ%-rel.txt >nul
)
)

@if NOT "%ADDDBG%x" == "1x" (
@echo NOTE: Debug build is DISABLED! >> %TMPLOG%
@goto DONE
)

@echo Do cmake Debug build >> %TMPLOG%

@echo Doing: 'cmake --build . --config Debug' >> %TMPLOG%
cmake --build . --config Debug >> %TMPLOG%
@echo Done: 'cmake --build . --config Debug' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOBLD

@echo Install and generate the Debug ZIP in a temporary folder >> %TMPLOG%
@if EXIST build-zip.bat (
@echo Doing 'call build-zip Debug' to generate %TMPPROJ% zip >> %TMPLOG%
call build-zip Debug >> %TMPLOG%
@if ERRORLEVEL 1 goto ZIPERR
) else (
@echo Note build-zip.bat does NOT exist in folder %CD%, so NO Debug ZIP created! >> %TMPLOG%
)

@if "%ADDINST%x" == "1x" (
@echo Doing: 'cmake -DBUILD_TYPE=Debug -P cmake_install.cmake' >> %TMPLOG%
cmake -DBUILD_TYPE=Debug -P cmake_install.cmake >> %TMPLOG%
@echo Done: 'cmake -DBUILD_TYPE=Debug -P cmake_install.cmake' ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@if ERRORLEVEL 1 goto NOINST

@echo. >> %TMPIFIL%
@echo = %TMPPROJ% Debug install %DATE% %TIME% >> %TMPIFIL%
@if EXIST install_manifest.txt (
type install_manifest.txt >> %TMPIFIL%
copy install_manifest.txt install-%TMPPROJ%-dbg.txt >nul
)
)

:DONE

@echo Looks successful - keep the CMakeLists.txt, in '%TMPCZIP%' >> %TMPLOG%
@if EXIST %TMPCZIP% del %TMPCZIP% >nul
@echo Doing: 'call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt' >> %TMPLOG%
@call zip8 -a -p -r -o %TMPCZIP% %TMPSRC%\CMakeLists.txt >nul 2>&1
@if EXIST %TMPCZIP% (
@call unzip8 -vb %TMPCZIP% >> %TMPLOG%
) else (
@echo WARNING: Appears ZIP of %TMPCZIP% failed, but considered non-critical! >> %TMPLOG%
)

@if NOT "%ADDINST%x" == "1x" (
@echo NOTE: Install to %TMPINST% is DISABLED >> %TMPLOG%
)
@call elapsed %TMPBGN% >> %TMPLOG%
@echo End %DATE% %TIME% ERRORLEVEL=%ERRORLEVEL% >> %TMPLOG%
@call elapsed %TMPBGN%
@echo End %DATE% %TIME% Success...
@goto END

:ZIPERR
@echo ERROR: build ZIP failed! returning ERRORLEVEL=%ERRORLEVEL%
@goto ISERR

:NOINST
@echo ERROR: CMake install error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOBLD
@echo ERROR: CMake build error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOCM
@echo ERROR: CMake configure and generation error! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@goto ISERR

:NOSRC
@echo ERROR: Could NOT locate SOURCE! %TMPSRC% >> %TMPLOG%
@goto ISERR

:NOSRC2
@echo ERROR: Could NOT locate SOURCE! %TMPSRC%\CMakeLists.txt >> %TMPLOG%
@goto ISERR

:NODIR
@echo ERROR: Could NOT create build directory %TMPBLD%! >> %TMPLOG%
@goto ISERR

:ISERR
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%! >> %TMPLOG%
@echo ERROREXIT! ERRORLEVEL=%ERRORLEVEL%!
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
