@setlocal
@echo off
@REM ===========================================================================================
@REM Build of 64-bit flightgear, and dependencies
@REM Required software: Visual Studio, CMake, SVN, GIT
@REM Here using MSVC10 PRO, standard install - see changes need \ed below for other versions
@REM ===========================================================================================
@REM ###########################################################################################
@REM Started with the 'more or less private (publicly available but not publicly announced)
@REM http://clement _DOT_ delhamaide _DOT_ free _DOT_ fr/download_and_compile.bat
@REM download_and_compile.bat v1.3.? - Clement de l'Hamaide - Oct 2013 - May 2014
@REM ##########################################################################################
@REM Then I started adding my own tweaks and takes to it ;=))
@REM Geoff R. McLane - June, 2014 - August, 2014 - v1.4.0 - makefg.x64..bat
@REM ##########################################################################################

REM ######################################################################################
REM ######################################################################################
REM #####    YOU SHOULD CHANGE FOLLOWING VARIABLES IN ACCORDANCE WITH YOUR SYSTEM    #####
REM ######################################################################################
REM ######################################################################################

set BUILD_BITS=%PROCESSOR_ARCHITECTURE%
REM set "BUILD_BITS=amd64"
REM set "BUILD_BITS=x86_amd64"
REM set "BUILD_BITS=x86"
set "GENERATOR=Visual Studio 10 Win64"
REM set "GENERATOR=Visual Studio 10"
set "VS_PATH=C:\Program Files (x86)\Microsoft Visual Studio 10.0"
REM set FG_ROOT=x:\fgdata
REM IF NOT EXIST %FG_ROOT%\version (
REM echo Unable to locate %FG_ROOT%\version
REM exit /b 1
REM )

REM ######################################################################################
REM ######################################################################################
REM ######################################################################################
REM ######################################################################################
REM ######################################################################################

REM ############################## SEARCH PATHS ##########################################
REM Search SVN.EXE path
set SVN_EXE=svn
CALL %SVN_EXE% --version --quiet
@if ERRORLEVEL 1 goto TRYSVNREG
@echo Found SVN
@goto GOTSVN
:TRYSVNREG
FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY HKLM\Software\TortoiseSVN /v Directory') DO SET SVN_EXE=%%Bbin\svn.exe
:GOTSVN
REM Search CMAKE.EXE path
set CMAKE_EXE=cmake
CALL %CMAKE_EXE% --version
@if ERRORLEVEL 1 goto TRYCMAKEREG
@echo Found CMAKE
@goto GOTCMAKE
:TRYCMAKEREG
FOR /F "tokens=1* delims=\" %%A IN ('REG QUERY HKLM\Software\Wow6432Node\Kitware') DO SET CMAKE_REG=HKLM\%%B
FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY "%CMAKE_REG%" /ve') DO SET CMAKE_EXE=%%B\bin\cmake.exe
IF NOT exist "%CMAKE_EXE%" (
	FOR /F "tokens=3* delims=	 " %%A IN ('REG QUERY "%CMAKE_REG%" /ve') DO SET CMAKE_EXE=%%B\bin\cmake.exe
)
:GOTCMAKE

REM Search GIT path
@set GIT_EXE=git
CALL %GIT_EXE% --version
@if ERRORLEVEL 1 goto TRYGITREG
@echo Found GIT
@goto GOTGIT
:TRYGITREG
FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY HKCU\Software\Git-Cheetah /v PathToMsys') DO SET string=%%B
SET GIT_PATH=%string:git-cheetah\..=bin%
:GOTGIT

REM Search CGAL path
@if "%CGAL_DIR%x" == "x" (
FOR /F "tokens=2* delims=	 " %%A IN ('REG QUERY HKCU\Environment /v CGAL_DIR') DO SET CGAL_PATH=%%B
@REM set "CGAL_PATH=C:\Program Files\CGAL-4.3"
) else (
@set "CGAL_PATH=%CGAL_DIR%"
)

REM ######################### SET EXECUTABLES PATH #######################################
REM set "CURL_EXE=%GIT_PATH%\curl.exe"
REM set "GIT_EXE=%GIT_PATH%\git.exe"
REM set "UNZIP_EXE=%GIT_PATH%\unzip.exe"
REM set "VC_BAT=%VS_PATH%\VC\vcvarsall.bat"
set CURL_EXE=wget
set CURL_OPTS=-O
set UNZIP_EXE=C:\MDOS\temp\unix\unzip.exe
set "VC_BAT=%VS_PATH%\VC\vcvarsall.bat"

REM #########################     SET REPO PATH     ######################################
set "BOOST_REPO=http://svn.boost.org/svn/boost/tags/release/Boost_1_55_0"
set "CGAL_REPO=https://gforge.inria.fr/frs/download.php/32358/CGAL-4.2.zip"
set "RDPARTY_REPO=http://fgfs.goneabitbursar.com/fgwin3rdparty/trunk/msvc100"
set "FG_REPO=git://gitorious.org/fg/flightgear.git"
set "TG_REPO=git://gitorious.org/fg/terragear.git"
set "TGGUI_REPO=git://gitorious.org/fgscenery/terrageargui.git"
set "SG_REPO=git://gitorious.org/fg/simgear.git"
set "FGRUN_REPO=git://gitorious.org/fg/fgrun.git"
set "FGDATA_REPO=git://mapserver.flightgear.org/fgdata"
set "FGSG_BRANCH=next"
set "FGDATA_BRANCH=master"
set "TG_BRANCH=scenery/ws2.0"
set "TGGUI_BRANCH=master"

REM #########################       GOTO HELP       ######################################
IF "%1"=="--help" GOTO Usage
IF "%1"=="-h" GOTO Usage
IF "%1"=="/h" GOTO Usage
IF "%1"=="/?" GOTO Usage
IF "%1"=="/help" GOTO Usage

REM ######################### CHECK AVAILABLE TOOLS ######################################
set "error=0"
IF NOT exist "%VS_PATH%" (
	echo.
    echo ERROR ! "%VS_PATH%" doesn't exist
    echo You must install a working MSVC2010 ^( Microsoft Visual Studio 2010 ^)
	set "error=1"
	echo.
)
IF NOT exist "%VC_BAT%" (
	echo.
    echo ERROR ! "%VC_BAT%" doesn't exist
    echo You must install a working MSVC2010 ^( Microsoft Visual Studio 2010 ^)
	set "error=1"
	echo.
)
IF "%CMAKE_EXE%x" == "x" (
	echo.
    echo ERROR ! "CMAKE.EXE can't be found in PATH or registry"
    echo You must install it ^( http://www.cmake.org/cmake/resources/software.html#latest ^)
	set "error=1"
	echo.
)
REM IF NOT exist "%CMAKE_EXE%" (
REM	echo.
REM    echo ERROR ! "%CMAKE_EXE%" doesn't exist
REM    echo You must install it ^( http://www.cmake.org/cmake/resources/software.html#latest ^)
REM	set "error=1"
REM	echo.
REM )

IF "%CGAL_PATH%"=="" (
echo.
echo ERROR ! "CGAL path can't be found in environment or registry"
echo You must install it ^( https://gforge.inria.fr/frs/download.php/32362/CGAL-4.2-Setup.exe ^)
set "error=1"
echo.
)
IF NOT exist "%CGAL_PATH%"\auxiliary\gmp\lib\libgmp*.lib (
echo.
echo ERROR ! LIBGMP doesn't exist at %CGAL_PATH%\auxiliary\gmp\lib\
echo You must install it ^( https://gforge.inria.fr/frs/download.php/32362/CGAL-4.2-Setup.exe ^)
set "error=1"
echo.
)

IF "%SVN_EXE%"=="" (
	echo.
    echo ERROR ! "SVN.EXE can't be found in PATH or registry"
    echo You must install it ^( http://tortoisesvn.net/downloads.html ^)
	set "error=1"
	echo.
)
REM IF NOT exist "%SVN_EXE%" (
REM	echo.
REM    echo ERROR ! "%SVN_EXE%" doesn't exist
REM    echo You must install it ^( http://tortoisesvn.net/downloads.html ^)
REM	set "error=1"
REM	echo.
REM )
REM IF "%GIT_PATH%"=="" (
REM	echo.
REM    echo ERROR ! "GIT path can't be found in registry"
REM    echo You must install it ^( http://git-scm.com/download/win ^)
REM	set "error=1"
REM	echo.
REM )
REM IF NOT exist "%GIT_EXE%" (
REM	echo.
REM    echo ERROR ! "%GIT_EXE%" doesn't exist
REM    echo You must install it ^( http://git-scm.com/download/win ^)
REM	set "error=1"
REM	echo.
REM )
REM IF NOT exist "%CURL_EXE%" (
REM	echo.
REM    echo ERROR ! "%CURL_EXE%" doesn't exist
REM	echo You must install it ^( http://git-scm.com/download/win ^)
REM	set "error=1"
REM	echo.
REM )
REM IF NOT exist "%UNZIP_EXE%" (
REM	echo.
REM    echo ERROR ! "%UNZIP_EXE%" doesn't exist
REM    echo You must install it ^( http://git-scm.com/download/win ^)
REM	set "error=1"
REM	echo. 
REM )
IF %error% EQU 1 (
	exit /b
)

REM ####################### SET 32/64 BITS ARCHITECTURE ##################################
IF /i %BUILD_BITS% EQU x86_amd64 (
    set "RDPARTY_ARCH=x64"
    set "RDPARTY_DIR=3rdParty.x64"
    set "OSG_REPO=http://flightgear.simpits.org:8080/view/Win/job/OSG-stable-Win64/lastSuccessfulBuild/artifact/install/msvc100-64/OpenSceneGraph/*zip*/OpenSceneGraph.zip"
) ELSE (
    IF /i %BUILD_BITS% EQU amd64 (
        set "RDPARTY_ARCH=x64"
        set "RDPARTY_DIR=3rdParty.x64"
        set "OSG_REPO=http://flightgear.simpits.org:8080/view/Win/job/OSG-stable-Win64/lastSuccessfulBuild/artifact/install/msvc100-64/OpenSceneGraph/*zip*/OpenSceneGraph.zip"
    ) ELSE (
        set "RDPARTY_ARCH=win32"
        set "RDPARTY_DIR=3rdParty"
        set "OSG_REPO=http://flightgear.simpits.org:8080/view/Win/job/OSG-stable-Win/lastSuccessfulBuild/artifact/install/msvc100/OpenSceneGraph/*zip*/OpenSceneGraph.zip"
    )
)
ECHO Setting environment - CALL "%VC_BAT%" %BUILD_BITS%
CALL "%VC_BAT%" %BUILD_BITS%

REM ############################      SET PATHS      #####################################
IF NOT exist install ( mkdir install )
IF NOT exist build ( mkdir build )
set "PWD=%CD%"
set "INSTALL_DIR=%PWD%\install"
set "OSG_INSTALL_DIR=%INSTALL_DIR%\OpenSceneGraph"
set "BOOST_INSTALL_DIR=%INSTALL_DIR%\Boost"
set "RDPARTY_INSTALL_DIR=%PWD%\%RDPARTY_DIR%"
set "SIMGEAR_INSTALL_DIR=%INSTALL_DIR%\SimGear"
set "FLIGHTGEAR_INSTALL_DIR=%INSTALL_DIR%\FlightGear"
set "FGDATA_INSTALL_DIR=%FLIGHTGEAR_INSTALL_DIR%\fgdata"
set "FGRUN_INSTALL_DIR=%INSTALL_DIR%\FGRun"
set "TERRAGEAR_INSTALL_DIR=%INSTALL_DIR%\TerraGear"
set "TGGUI_INSTALL_DIR=%INSTALL_DIR%\TerraGearGUI"
set "CGAL_INSTALL_DIR=%INSTALL_DIR%\CGAL"
REM IF "%FG_ROOT%x" == "x" goto DN_FGROOT
REM IF NOT EXIST %FG_ROOT%\nul goto DN_FGROOT
REM IF NOT EXIST %FG_ROOT%\version goto DN_FGROOT
REM set /p "FGVER=" < %FG_ROOT%\version
REM set FGDATA_INSTALL_DIR=%FG_ROOT%
REM echo set FGDATA_INSTALL_DIR to %FG_ROOT% version %FGVER%
REM :DN_FGROOT

@set LOGFIL=%PWD%\build\bldlog-1.txt
@echo Begin build %DATE% %TIME% > %LOGFIL%
@set BLDLOG=
@set HAVELOG=0
@REM Uncomment this, and add %BLDLOG% to config/build lines, if you want output to a LOG
@set BLDLOG= ^>^> %LOGFIL% 2^>^&1
@set HAVELOG=1

REM ###########################      SET OPTIONS      ####################################
set "PULL=1"
set "CMAKE=1"
set "COMPILE=1"
set DO3RD=1

REM ###################### DOWNLOAD FGDATA IN BACKGROUND #################################
IF "%1"=="" (
    set BUILD_ALL=1
    IF NOT exist "%FLIGHTGEAR_INSTALL_DIR%" ( mkdir "%FLIGHTGEAR_INSTALL_DIR%" )
    echo Uncomment this is you need to clone fgdata
    REM echo Start FGDATA download in background
    REM START "" /MIN cmd /C "%PWD%"\download_and_compile.bat fgdata
) ELSE (
    set BUILD_ALL=0
)

REM ###########################    PARSE ARGUMENTS    ####################################
:Parser
IF %BUILD_ALL% EQU 0 (
    IF "%1"=="" (
        GOTO finished
    )
	IF "%1" == "/P" (
		set "PULL=0"
		SHIFT
		GOTO Parser
	)
	IF "%1" == "/M" (
		set "CMAKE=0"
		SHIFT
		GOTO Parser
	)
	IF "%1" == "/C" (
		set "COMPILE=0"
		SHIFT
		GOTO Parser
	)
	IF "%1" == "/3" (
		set DO3RD=0
		SHIFT
		GOTO Parser
	)
    ECHO GOTO %1
    IF %HAVELOG% EQU 1 (ECHO GOTO %1 %BLDLOG%)
	GOTO %1
)

:3rdparty
echo ##############################
echo #########  3RDPARTY  #########
IF %HAVELOG% EQU 1 (echo #########  3RDPARTY  ######### %BLDLOG%)
echo ##############################
@REM This could be replaced by BUILDING all the 3rdParty components
cd "%PWD%"
IF NOT exist %PWD%\%RDPARTY_DIR%\nul (
    echo Cloning "%RDPARTY_REPO%/%RDPARTY_DIR%"...
    CALL %SVN_EXE% co %RDPARTY_REPO%/%RDPARTY_DIR% %RDPARTY_DIR%
) else (
    ECHO %PWD%\%RDPARTY_DIR% directory exists, so no svn of simpits done
)

cd "%PWD%"
echo Done 3RDPARTY from simpits...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:boost
echo ##############################
echo ##########  BOOST  ###########
echo ##############################
cd "%PWD%"
IF NOT exist "%PWD%"\boost (
    echo Cloning "%BOOST_REPO%"...
    CALL "%SVN_EXE%" co "%BOOST_REPO%" boost
)
cd "%PWD%"\boost
IF %COMPILE% EQU 1 (
    echo Checking BOOST_INSTALL_DIR=%BOOST_INSTALL_DIR%
	IF NOT exist %BOOST_INSTALL_DIR% (
        ECHO Doing: 'call .\bootstrap' %BLDLOG%
        IF %HAVELOG% EQU 1 (
            ECHO Doing: 'call .\bootstrap' to %LOGFIL%
        )
		call .\bootstrap %BLDLOG%
        IF /i %RDPARTY_ARCH% EQU x64 (
			ECHO Doing: '.\b2 install --prefix="%BOOST_INSTALL_DIR%" address-model=64' %BLDLOG%
            IF %HAVELOG% EQU 1 (
                ECHO Doing: '.\b2 install --prefix="%BOOST_INSTALL_DIR%" address-model=64' to %LOGFIL%
            )
			.\b2 install --prefix="%BOOST_INSTALL_DIR%" address-model=64 %BLDLOG%
        ) ELSE (
			ECHO Doing: '.\b2 install --prefix="%BOOST_INSTALL_DIR%"' %BLDLOG%
            IF %HAVELOG% EQU 1 (
                ECHO Doing: '.\b2 install --prefix="%BOOST_INSTALL_DIR%"' to %LOGFIL%
            )
			.\b2 install --prefix="%BOOST_INSTALL_DIR%" %BLDLOG%
		)
	)
	) else (
        echo BOOST already done! Remove %BOOST_INSTALL_DIR% to redo... %BLDLOG%
        IF %HAVELOG% EQU 1 (
            echo BOOST already done! Remove %BOOST_INSTALL_DIR% to redo...
        )
    )
)
cd "%PWD%"
echo Done BOOST...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:cgal
echo ##############################
echo ###########  CGAL  ###########
echo ##############################

IF NOT exist %CGAL_INSTALL_DIR% (
	cd "%PWD%"\build
	IF NOT exist cgal (mkdir cgal)
	cd cgal
	IF %CMAKE% EQU 1 (
		DEL CMakeCache.txt 2>nul
		ECHO "Doing: 'CALL %CMAKE_EXE% %CGAL_PATH% -G %GENERATOR% -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=%CGAL_INSTALL_DIR% -DCMAKE_PREFIX_PATH=%CGAL_PATH%\auxiliary\gmp;%BOOST_INSTALL_DIR% -DCGAL_Boost_USE_STATIC_LIBS:BOOL=ON -DZLIB_LIBRARY=%RDPARTY_INSTALL_DIR%\lib\zlib.lib -DZLIB_INCLUDE_DIR=%RDPARTY_INSTALL_DIR%\include'" %BLDLOG%
        IF %HAVELOG% EQU 1 (
            ECHO "Doing: 'CALL %CMAKE_EXE% %CGAL_PATH% -G %GENERATOR% -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=%CGAL_INSTALL_DIR% -DCMAKE_PREFIX_PATH=%CGAL_PATH%\auxiliary\gmp;%BOOST_INSTALL_DIR% -DCGAL_Boost_USE_STATIC_LIBS:BOOL=ON -DZLIB_LIBRARY=%RDPARTY_INSTALL_DIR%\lib\zlib.lib -DZLIB_INCLUDE_DIR=%RDPARTY_INSTALL_DIR%\include'" to %LOGFIL%
        )
		CALL "%CMAKE_EXE%" "%CGAL_PATH%" ^
			-G "%GENERATOR%" ^
			-DCMAKE_BUILD_TYPE="Release" ^
			-DCMAKE_INSTALL_PREFIX:PATH="%CGAL_INSTALL_DIR%" ^
			-DCMAKE_PREFIX_PATH="%CGAL_PATH%\auxiliary\gmp;%BOOST_INSTALL_DIR%" ^
			-DCGAL_Boost_USE_STATIC_LIBS:BOOL=ON ^
			-DZLIB_LIBRARY="%RDPARTY_INSTALL_DIR%\lib\zlib.lib" ^
			-DZLIB_INCLUDE_DIR="%RDPARTY_INSTALL_DIR%\include" %BLDLOG%
	)
	IF %COMPILE% EQU 1 (
		ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' %BLDLOG%
        IF %HAVELOG% EQU 1 (
            ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' to %LOGFIL%
        )
		CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL %BLDLOG%
	)
)
cd "%PWD%"
echo Done CGAL...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:osg
echo ##############################
echo ###########  OSG  ############
echo ##############################

cd %PWD%
set "OSG_ZIP=osg.zip"
IF NOT exist %OSG_ZIP% (
    echo Downloading %OSG_REPO%...
    CALL "%CURL_EXE%" %CURL_OPTS% %OSG_ZIP% %OSG_REPO%
) else (
    echo Found %OSG_ZIP%
)
IF NOT exist %OSG_ZIP% (
    echo ERROR: %OSG_ZIP% is missing: download failed
    GOTO finished
)
IF NOT exist "%OSG_INSTALL_DIR%" (
    ECHO Doing: CALL "%UNZIP_EXE%" %OSG_ZIP% -d "%INSTALL_DIR%"
    CALL "%UNZIP_EXE%" %OSG_ZIP% -d "%INSTALL_DIR%"
) else (
    ECHO Found "%OSG_INSTALL_DIR%"
)
cd "%PWD%"
echo Done OSG...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

IF %DO3RD% EQU 1 (
    IF EXIST build_%RDPARTY_DIR%.bat (
        ECHO Building %RDPARTY_DIR% from sources %BLDLOG%
        CALL build_%RDPARTY_DIR%
        IF EXIST bldlog-2.txt (
            type bldlog-2.txt >> %LOGFIL%
        )
        ECHO Done %RDPARTY_DIR% build from sources %BLDLOG%
    ) ELSE (
        ECHO File 'build_%RDPARTY_DIR%.bat' NOT FOUND! No update of %RDPARTY_DIR% folder %BLDLOG%
    )
) ELSE (
    ECHO Skipping %RDPARTY_DIR% update %BLDLOG%
)

:simgear
echo ##############################
echo #########  SIMGEAR  ##########
echo ##############################

IF exist "%PWD%"\simgear (
	CALL :_gitUpdate simgear
) ELSE (
    echo Cloning "%SG_REPO%"...
    REM CALL %GIT_EXE% clone -- %SG_REPO% - what is this '--'???
    echo Doing: CALL %GIT_EXE% clone %SG_REPO%
    CALL %GIT_EXE% clone %SG_REPO%
)

cd "%PWD%"\build
IF NOT exist simgear (mkdir simgear)
cd simgear
IF %CMAKE% EQU 1 (
	DEL CMakeCache.txt 2>nul
    @REM Build DEBUG libraries
	ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\simgear -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Debug" -DENABLE_TESTS=OFF -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%SIMGEAR_INSTALL_DIR%" %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\simgear -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Debug" -DENABLE_TESTS=OFF -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%SIMGEAR_INSTALL_DIR%" to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" ..\..\simgear ^
		-G "%GENERATOR%" ^
		-DCMAKE_BUILD_TYPE="Debug" ^
		-DENABLE_TESTS=OFF ^
		-DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" ^
		-DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" ^
		-DBOOST_ROOT="%INSTALL_DIR%" ^
		-DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" ^
		-DCMAKE_INSTALL_PREFIX:PATH="%SIMGEAR_INSTALL_DIR%" %BLDLOG%
IF %COMPILE% EQU 1 (
    @REM Install DEBUG
	ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Debug --target INSTALL' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Debug --target INSTALL' to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" --build . --config Debug --target INSTALL %BLDLOG%
)
        
    @REM Build RELEASE libraries
	ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\simgear -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DENABLE_TESTS=OFF -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%SIMGEAR_INSTALL_DIR%" %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\simgear -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DENABLE_TESTS=OFF -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%SIMGEAR_INSTALL_DIR%" to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" ..\..\simgear ^
		-G "%GENERATOR%" ^
		-DCMAKE_BUILD_TYPE="Release" ^
		-DENABLE_TESTS=OFF ^
		-DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" ^
		-DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" ^
		-DBOOST_ROOT="%INSTALL_DIR%" ^
		-DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" ^
		-DCMAKE_INSTALL_PREFIX:PATH="%SIMGEAR_INSTALL_DIR%" %BLDLOG%
)
IF %COMPILE% EQU 1 (
    @REM Install RELEASE
	ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL %BLDLOG%
)
cd "%PWD%"
echo Done SIMGEAR...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:flightgear
echo ##############################
echo ########  FLIGHTGEAR  ########
echo ##############################

IF exist "%PWD%"\flightgear (
	CALL :_gitUpdate flightgear
) ELSE ( 
    echo Cloning %FG_REPO%...
    CALL %GIT_EXE% clone %FG_REPO%
)

cd "%PWD%"\build
IF NOT exist flightgear (mkdir flightgear)
cd flightgear
IF %CMAKE% EQU 1 (
	DEL CMakeCache.txt 2>nul
	ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\flightgear -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DWITH_FGPANEL=OFF -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%SIMGEAR_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%FLIGHTGEAR_INSTALL_DIR%" %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\flightgear -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DWITH_FGPANEL=OFF -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%SIMGEAR_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%FLIGHTGEAR_INSTALL_DIR%" to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" ..\..\flightgear ^
		-G "%GENERATOR%" ^
		-DCMAKE_BUILD_TYPE="Release" ^
		-DWITH_FGPANEL=OFF ^
		-DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" ^
		-DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" ^
		-DBOOST_ROOT="%INSTALL_DIR%" ^
		-DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%SIMGEAR_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" ^
		-DCMAKE_INSTALL_PREFIX:PATH="%FLIGHTGEAR_INSTALL_DIR%" %BLDLOG%
)
IF %COMPILE% EQU 1 ( 
	ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL %BLDLOG%
)
cd "%PWD%"

xcopy "%OSG_INSTALL_DIR%"\bin\*.dll "%FLIGHTGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\zlib.dll "%FLIGHTGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\libpng16.dll "%FLIGHTGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\OpenAL32.dll "%FLIGHTGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\CrashRpt1402.dll "%FLIGHTGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\CrashSender1402.exe "%FLIGHTGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\crashrpt_lang.ini "%FLIGHTGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q

REM create an enhanced run_fgfs.bat
echo @echo off > run_fgfs.bat
echo setlocal >> run_fgfs.bat
echo set TMPCMD= >> run_fgfs.bat
echo :RPT >> run_fgfs.bat
echo if "%%~1x" == "x" goto GOTCMD >> run_fgfs.bat
echo set TMPCMD=%%TMPCMD%% %%1 >> run_fgfs.bat 
echo shift >> run_fgfs.bat
echo goto RPT >> run_fgfs.bat
echo :GOTCMD >> run_fgfs.bat 
echo set TMPRT=%FGDATA_INSTALL_DIR% >> run_fgfs.bat
echo if NOT EXIST %%TMPRT%% ( >> run_fgfs.bat
echo echo Error: Can NOT locate %%TMPRT%%! *** FIX ME *** >> run_fgfs.bat
echo exit /b 1 >> run_fgfs.bat
echo ) >> run_fgfs.bat
echo for /F "eol=# tokens=*" %%%%G in (fgfsrc) do CALL :concat "%%%%G" >> run_fgfs.bat
echo start /d "%FLIGHTGEAR_INSTALL_DIR%\bin" fgfs.exe --fg-root=%%TMPRT%% %%ARGUMENTS%% %%TMPCMD%% >> run_fgfs.bat
echo goto :eof >> run_fgfs.bat
echo :concat >> run_fgfs.bat
echo set "ARGUMENTS=%%ARGUMENTS%% %%~1" >> run_fgfs.bat
echo goto :eof >> run_fgfs.bat

IF NOT exist fgfsrc ( 
	echo # Write one argument per line then run "run_fgfs.bat" > fgfsrc
	echo --console >> fgfsrc
)

echo Done FLIGHTGEAR...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:terragear
echo ##############################
echo ########  TERRAGEAR  #########
echo ##############################

IF exist "%PWD%"\terragear (
	CALL :_gitUpdate terragear
) ELSE (
    echo Cloning "%TG_REPO%"...
    CALL %GIT_EXE% clone "%TG_REPO%"
)

cd "%PWD%"\terragear
CALL %GIT_EXE% checkout "%TG_BRANCH%"

cd "%PWD%"\build
IF NOT exist terragear (mkdir terragear)
cd terragear
IF %CMAKE% EQU 1 (
	DEL CMakeCache.txt 2>nul
    ECHO Doing: 'CALL %CMAKE_EXE% ..\..\terragear -G %GENERATOR% -DCMAKE_BUILD_TYPE="Release" -DBOOST_ROOT=%BOOST_INSTALL_DIR% -DJPEG_LIBRARY=%RDPARTY_INSTALL_DIR%\lib\jpeg.lib -DCMAKE_INSTALL_PREFIX:PATH=%TERRAGEAR_INSTALL_DIR% -DCMAKE_PREFIX_PATH=%SIMGEAR_INSTALL_DIR%;%CGAL_INSTALL_DIR%;%BOOST_INSTALL_DIR%' %BLDLOG% 
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL %CMAKE_EXE% ..\..\terragear -G %GENERATOR% -DCMAKE_BUILD_TYPE="Release" -DBOOST_ROOT=%BOOST_INSTALL_DIR% -DJPEG_LIBRARY=%RDPARTY_INSTALL_DIR%\lib\jpeg.lib -DCMAKE_INSTALL_PREFIX:PATH=%TERRAGEAR_INSTALL_DIR% -DCMAKE_PREFIX_PATH=%SIMGEAR_INSTALL_DIR%;%CGAL_INSTALL_DIR%;%BOOST_INSTALL_DIR%' to %LOGFIL% 
    )
	CALL "%CMAKE_EXE%" ..\..\terragear ^
		-G "%GENERATOR%" ^
		-DCMAKE_BUILD_TYPE="Release" ^
		-DBOOST_ROOT="%BOOST_INSTALL_DIR%" ^
		-DJPEG_LIBRARY="%RDPARTY_INSTALL_DIR%\lib\jpeg.lib" ^
		-DCMAKE_INSTALL_PREFIX:PATH="%TERRAGEAR_INSTALL_DIR%" ^
		-DCMAKE_PREFIX_PATH="%SIMGEAR_INSTALL_DIR%;%CGAL_INSTALL_DIR%;%BOOST_INSTALL_DIR%" %BLDLOG%
)
IF %COMPILE% EQU 1 (
	ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL %BLDLOG%
)
cd "%PWD%"

IF /i %RDPARTY_ARCH% EQU x64 (
	xcopy "%RDPARTY_INSTALL_DIR%"\bin\iconv_x64.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
	COPY "%TERRAGEAR_INSTALL_DIR%"\bin\iconv_x64.dll "%TERRAGEAR_INSTALL_DIR%"\bin\iconv.dll
) ELSE (
	xcopy "%RDPARTY_INSTALL_DIR%"\bin\iconv.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
)
IF EXIST "%RDPARTY_INSTALL_DIR%\bin\gdal200.dll" (
ECHO xcopy "%RDPARTY_INSTALL_DIR%\bin\gdal200.dll" to "%TERRAGEAR_INSTALL_DIR%\bin\*"
xcopy "%RDPARTY_INSTALL_DIR%\bin\gdal200.dll" "%TERRAGEAR_INSTALL_DIR%\bin\*" /s /e /i /Y /q
) ELSE (
    IF EXIST "%RDPARTY_INSTALL_DIR%\bin\gdal17.dll" (
ECHO xcopy "%RDPARTY_INSTALL_DIR%\bin\gdal17.dll" "%TERRAGEAR_INSTALL_DIR%\bin\*"
xcopy "%RDPARTY_INSTALL_DIR%\bin\gdal17.dll" "%TERRAGEAR_INSTALL_DIR%\bin\*" /s /e /i /Y /q
    ) ELSE (
        IF EXIST "%RDPARTY_INSTALL_DIR%\bin\gdal111.dll" (
ECHO xcopy "%RDPARTY_INSTALL_DIR%\bin\gdal111.dll" "%TERRAGEAR_INSTALL_DIR%\bin\*"
xcopy "%RDPARTY_INSTALL_DIR%\bin\gdal111.dll" "%TERRAGEAR_INSTALL_DIR%\bin\*" /s /e /i /Y /q
        ) ELSE (
ECHO WARNING: No GDAL DLL found in "%RDPARTY_INSTALL_DIR%\bin"!
            IF %HAVELOG% EQU 1 (
                ECHO WARNING: No GDAL DLL found in "%RDPARTY_INSTALL_DIR%\bin"! %BLDLOG%
            )
        )
    )
)
xcopy "%RDPARTY_INSTALL_DIR%"\bin\xerces-c_2_8.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\libexpat.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\libpq.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\spatialite.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\proj.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\geos_c.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\libcurl.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%CGAL_PATH%"\auxiliary\gmp\lib\*.dll "%TERRAGEAR_INSTALL_DIR%"\bin\* /s /e /i /Y /q
echo Done TERRAGEAR...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:terrageargui
echo ##############################
echo ######  TERRAGEARGUI  ########
echo ##############################

IF exist "%PWD%"\terrageargui (
	CALL :_gitUpdate terrageargui
) ELSE (
    echo Cloning "%TGGUI_REPO%"...
    CALL %GIT_EXE% clone "%TGGUI_REPO%"
)

cd "%PWD%"\build
IF NOT exist terrageargui (mkdir terrageargui)
cd terrageargui
IF %CMAKE% EQU 1 (
	DEL CMakeCache.txt 2>nul
	ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\terrageargui -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DCMAKE_INSTALL_PREFIX:PATH="%TGGUI_INSTALL_DIR%"' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\terrageargui -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DCMAKE_INSTALL_PREFIX:PATH="%TGGUI_INSTALL_DIR%"' to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" ..\..\terrageargui ^
		-G "%GENERATOR%" ^
		-DCMAKE_BUILD_TYPE="Release" ^
		-DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" ^
		-DCMAKE_INSTALL_PREFIX:PATH="%TGGUI_INSTALL_DIR%" %BLDLOG%
)
IF %COMPILE% EQU 1 (
	ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' to %LOGFIL%
    )    
	CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL %BLDLOG%
)
cd "%PWD%"
echo Done TERRAGEARGUI...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:fgrun
echo ##############################
echo ########     FGRUN    ########
echo ##############################

IF exist "%PWD%"\fgrun (
	CALL :_gitUpdate fgrun
) ELSE ( 
    echo Cloning %FGRUN_REPO%...
    CALL %GIT_EXE% clone %FGRUN_REPO%
)

cd "%PWD%"\build
IF NOT exist fgrun (mkdir fgrun)
cd fgrun
IF %CMAKE% EQU 1 (
	DEL CMakeCache.txt 2>nul
	ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\fgrun -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%SIMGEAR_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%FGRUN_INSTALL_DIR%"' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" ..\..\fgrun -G "%GENERATOR%" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" -DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" -DBOOST_ROOT="%INSTALL_DIR%" -DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%SIMGEAR_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" -DCMAKE_INSTALL_PREFIX:PATH="%FGRUN_INSTALL_DIR%"' to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" ..\..\fgrun ^
		-G "%GENERATOR%" ^
		-DCMAKE_BUILD_TYPE="Release" ^
		-DCMAKE_EXE_LINKER_FLAGS="/SAFESEH:NO" ^
		-DMSVC_3RDPARTY_ROOT="%RDPARTY_INSTALL_DIR%" ^
		-DBOOST_ROOT="%INSTALL_DIR%" ^
		-DCMAKE_PREFIX_PATH="%BOOST_INSTALL_DIR%;%OSG_INSTALL_DIR%;%SIMGEAR_INSTALL_DIR%;%RDPARTY_INSTALL_DIR%" ^
		-DCMAKE_INSTALL_PREFIX:PATH="%FGRUN_INSTALL_DIR%" %BLDLOG%
)
IF %COMPILE% EQU 1 ( 
	ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' %BLDLOG%
    IF %HAVELOG% EQU 1 (
        ECHO Doing: 'CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL' to %LOGFIL%
    )
	CALL "%CMAKE_EXE%" --build . --config Release --target INSTALL %BLDLOG%
)
cd %PWD%
xcopy "%OSG_INSTALL_DIR%"\bin\*.dll "%FGRUN_INSTALL_DIR%"\bin\* /s /e /i /Y /q
xcopy "%RDPARTY_INSTALL_DIR%"\bin\libintl-8.dll "%FGRUN_INSTALL_DIR%"\bin\* /s /e /i /Y /q

echo @echo off > run_fgrun.bat
echo start /d "%FGRUN_INSTALL_DIR%\bin" fgrun.exe >> run_fgrun.bat
echo Done FGRUN...

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
) else (
	GOTO finished
)

:fgdata
echo ##############################
echo ##########  FGDATA  ##########
echo ##############################
IF "%FG_ROOT%x" == "x" goto DO_FGDATA
echo Have a FG_ROOT=%FG_ROOT%
GOTO DN_FGDATA
:DO_FGDATA
IF exist %FGDATA_INSTALL_DIR% (
	CALL :_gitUpdate install/flightgear/fgdata
) ELSE (
    echo Cloning %FGDATA_REPO%...
    cd "%FLIGHTGEAR_INSTALL_DIR%"
    CALL %GIT_EXE% clone %FGDATA_REPO%
)
:DN_FGDATA

IF %BUILD_ALL% EQU 0 (
    SHIFT
    GOTO Parser
)

:finished
IF %HAVELOG% EQU 1 (
    ECHO See output in %LOGFIL% 
)
echo #########  F         #########
echo #########   I        #########
echo #########    N       #########
echo #########     I      #########
echo #########      S     #########
echo #########       H    #########
echo #########        E   #########
echo #########         D  #########
exit /b

:Usage
echo Usage: 
echo    .\download_and_compile.bat [Options] [Targets]
echo    Options:
echo       /C  : Do not compile
echo       /M  : Do not run cmake
echo       /P  : Do not run git pull
echo       /3  : Do not update 3rdparty folder
echo    Targets: Can be one or more of
echo    3rdparty boost osg simgear flightgear fgrun fgdata terragear terrageargui
echo    If no targets specified then all will be done.
echo.
echo    Don't forget to edit the top of the script in accordance with your system
exit /b

REM ###########################    HELPER FUNCTION    ####################################
:_gitUpdate
    IF %PULL% EQU 1 (
		echo Pulling %1...
		cd "%PWD%"\%1
		CALL %GIT_EXE% stash save
		REM CALL %GIT_EXE% pull -r - what is this -r???
		CALL %GIT_EXE% pull
		CALL %GIT_EXE% stash pop
		cd "%PWD%"
	)
GOTO :EOF

@REM EOF
@REM EOF
