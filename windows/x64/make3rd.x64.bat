@setlocal
@REM ================================================================================
@REM Build 3rdParty components prior to building flightgear
@REM ================================================================================
@REM ################################################################################
@REM started with from : http://wiki.flightgear.org/Howto:Build_3rdParty_library_for_Windows
@REM ################################################################################
@REM Renamed build3rd.x64.bat - v1.0.1 - 20140811
@REM ################################################################################
@set "WORKSPACE=%CD%"

@set GET_EXE=wget
@set GET_OPT=-O
@set UZ_EXE=7z
@set UZ_OPT=x
@set MOV_CMD=move
@set MOV_OPT=
@set SET_BAT="%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\VC\vcvarsall.bat"
@set HAD_ERROR=0

@set TMP3RD=3rdParty.x64
@set PERL_FIL=%WORKSPACE%\rep32w64.pl
@set LOGFIL=%WORKSPACE%\bldlog-2.txt
@set BLDLOG=
@REM Uncomment this, and add to config/build line, if you can output to a LOG
@set BLDLOG= ^>^> %LOGFIL% 2^>^&1
@set ERRLOG=%WORKSPACE%\error-2.txt
@set ADD_GDAL=0
@set HAVELOG=1

@echo %0: Begin %DATE% %TIME% in %CD% > %LOGFIL%
@echo # Error log %DATE% %TIME% > %ERRLOG%

@REM #############################################################################
@REM #############################################################################
@REM #### CGAL SETUP - THIS MAY NEED TO BE CHANGED TO WHERE YOU INSTALL CGAL #####
@REM #############################################################################
@REM #############################################################################
@if "%CGAL_DIR%x" == "x" (
@set "CGAL_PATH=C:\Program Files\CGAL-4.3"
) else (
@set "CGAL_PATH=%CGAL_DIR%"
)

@set "GMP_HDR=%CGAL_PATH%\auxiliary\gmp\include\gmp.h"
@set "GMP_DLL=%CGAL_PATH%\auxiliary\gmp\lib\libgmp-10.dll"
@set "GMP_LIB=%CGAL_PATH%\auxiliary\gmp\lib\libgmp-10.lib"
@if NOT EXIST %CGAL_PATH%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Can NOT locate %CGAL_PATH%! *** FIX ME ***
@echo %HAD_ERROR%: Can NOT locate %CGAL_PATH%! *** FIX ME *** >> %ERRLOG%
)
@if NOT EXIST %GMP_DLL% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Can NOT locate %GMP_DLL%! *** FIX ME ***
@echo %HAD_ERROR%: Can NOT locate %GMP_DLL%! *** FIX ME *** >> %ERRLOG%
)
@if NOT EXIST %GMP_LIB% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Can NOT locate %GMP_LIB%! *** FIX ME ***
@echo %HAD_ERROR%: Can NOT locate %GMP_LIB%! *** FIX ME *** >> %ERRLOG%
)

@if NOT %HAD_ERROR% EQU 0 goto END

@REM ######################################################################
@REM ######################################################################
@REM ########### SHOULD NOT NEED TO ALTER ANYTHING BELOW HERE ############# 
@REM ######################################################################

@if NOT EXIST  %WORKSPACE%\%TMP3RD%\nul (
md %WORKSPACE%\%TMP3RD%
)
@if NOT EXIST %WORKSPACE%\%TMP3RD%\bin\nul (
md %WORKSPACE%\%TMP3RD%\bin
)
@if NOT EXIST %WORKSPACE%\%TMP3RD%\lib\nul (
md %WORKSPACE%\%TMP3RD%\lib
)
@if NOT EXIST %WORKSPACE%\%TMP3RD%\include\nul (
md %WORKSPACE%\%TMP3RD%\include
)
 
CALL %SET_BAT% amd64

@REM TEST JUMP
@REM GOTO DO_CGAL
@REM GOTO DO_GDAL 
 
:DO_ZLIB
@echo %0: ############################# Download ^& compile ZLIB %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile ZLIB
)

@set TMP_URL=http://zlib.net/zlib128.zip
@set TMP_ZIP=zlib.zip
@set TMP_SRC=zlib-source
@set TMP_DIR=zlib-1.2.8
@set TMP_BLD=zlib-build

@if NOT EXIST zlib.zip ( 
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
@REM Seems NEED a delay after the UNZIP, else get access denied on the renaming???
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_ZLIB
)

cd %WORKSPACE%

@if NOT EXIST %TMP_BLD%\nul (
md %TMP_BLD%
)

CD %TMP_BLD%

ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\zlib-build\build" %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\zlib-build\build"
)
cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\zlib-build\build" %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit config/gen %TMP_SRC%
@echo %HAD_ERROR%: Error exit config/gen %TMP_SRC% >> %ERRLOG%
)

ECHO Doing 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing 'cmake --build . --config Release --target INSTALL'
)
cmake --build . --config Release --target INSTALL
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)
 
xcopy %WORKSPACE%\zlib-build\build\include\* %WORKSPACE%\%TMP3RD%\include /y /s /q
xcopy %WORKSPACE%\zlib-build\build\lib\zlib.lib %WORKSPACE%\%TMP3RD%\lib /y /q
xcopy %WORKSPACE%\zlib-build\build\bin\zlib.dll %WORKSPACE%\%TMP3RD%\bin /y /q

:DN_ZLIB
cd %WORKSPACE%

:DO_TIFF
@echo %0: ############################# Download ^& compile LIBTIFF %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBTIFF to %LOGFIL%
)

@set TMP_URL=http://download.osgeo.org/libtiff/tiff-4.0.3.zip
@set TMP_ZIP=libtiff.zip
@set TMP_SRC=libtiff-source
@set TMP_DIR=tiff-4.0.3

@if NOT EXIST %TMP_ZIP% ( 
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_TIFF
)

cd %TMP_SRC%
ECHO Doing: 'nmake -f makefile.vc' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'nmake -f makefile.vc'
)
nmake -f makefile.vc
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)

cd %WORKSPACE%

xcopy %WORKSPACE%\libtiff-source\libtiff\libtiff.lib %WORKSPACE%\%TMP3RD%\lib\ /y /f
xcopy %WORKSPACE%\libtiff-source\libtiff\libtiff_i.lib %WORKSPACE%\%TMP3RD%\lib\ /y /f
xcopy %WORKSPACE%\libtiff-source\libtiff\libtiff.dll %WORKSPACE%\%TMP3RD%\bin\ /y /f
xcopy %WORKSPACE%\libtiff-source\libtiff\tiff.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libtiff-source\libtiff\tiffconf.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libtiff-source\libtiff\tiffio.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libtiff-source\libtiff\tiffvers.h %WORKSPACE%\%TMP3RD%\include\ /y /f

:DN_TIFF
cd %WORKSPACE%

:DO_PNG
 
@echo %0: ############################# Download ^& compile LIBPNG %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBPNG to %LOGFIL%
)

@set TMP_URL=http://download.sourceforge.net/libpng/lpng1610.zip
@set TMP_ZIP=libpng.zip
@set TMP_SRC=libpng-source
@set TMP_DIR=lpng1610
@set TMP_BLD=libpng-build

@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_PNG
)

cd %WORKSPACE%

@if NOT EXIST %TMP_BLD%\nul (
MD %TMP_BLD%
)

CD %TMP_BLD%
ECHO Doing 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libpng-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%"' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libpng-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%"' to %LOGFIL%
)

cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libpng-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%"
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit cmake config/gen %TMP_SRC%
@echo %HAD_ERROR%: Error exit cmake config/gen %TMP_SRC% >> %ERRLOG%
)
ECHO Doing 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing 'cmake --build . --config Release --target INSTALL' to %LOGFIL%
)

cmake --build . --config Release --target INSTALL %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)

xcopy %WORKSPACE%\libpng-build\build\include\*.h %WORKSPACE%\%TMP3RD%\include /y
xcopy %WORKSPACE%\libpng-build\build\lib\libpng16.lib %WORKSPACE%\%TMP3RD%\lib /y
xcopy %WORKSPACE%\libpng-build\build\bin\libpng16.dll %WORKSPACE%\%TMP3RD%\bin /y

:DN_PNG
cd %WORKSPACE%
:DO_JPEG

@echo %0: ############################# Download ^& compile LIBJPEG %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBJPEG to %LOGFIL%
)

@set TMP_URL=http://www.ijg.org/files/jpegsr9a.zip
@set TMP_ZIP=libjpeg.zip
@set TMP_SRC=libjpeg-source
@set TMP_DIR=jpeg-9a

@REM ### setup a perl script
@if EXIST %PERL_FIL% goto DN_PFIL
@echo Creating %PERL_FIL%...
@echo #!/usr/bin/perl -w >%PERL_FIL%
@echo # rep32w64.pl >>%PERL_FIL%
@echo. >>%PERL_FIL%
@echo if (@ARGV) { >>%PERL_FIL%
@echo   my $file = $ARGV[0]; >>%PERL_FIL%
@echo   if (open(INF,"<$file")) { >>%PERL_FIL%
@echo     my @lines = ^<INF^>; >>%PERL_FIL%
@echo     close INF; >>%PERL_FIL%
@echo     my ($line,$lncnt,$i); >>%PERL_FIL%
@echo     $lncnt = scalar @lines; >>%PERL_FIL%
@echo     for ($i = 0; $i ^< $lncnt; $i++) { >>%PERL_FIL%
@echo 	      $line = $lines[$i]; >>%PERL_FIL%
@echo         $line =~ s/Win32/x64/g; >>%PERL_FIL%
@echo         $lines[$i] = $line; >>%PERL_FIL%
@echo     } >>%PERL_FIL%
@echo     if (open WOF, ">$file") { >>%PERL_FIL%
@echo 	    print WOF join("",@lines)."\n"; >>%PERL_FIL%
@echo 	    close WOF; >>%PERL_FIL%
@echo       exit(0); >>%PERL_FIL%
@echo     } >>%PERL_FIL%
@echo   } >>%PERL_FIL%
@echo } >>%PERL_FIL%
@echo exit(1); >>%PERL_FIL%
@echo # eof >>%PERL_FIL%
:DN_PFIL

@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)

@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to FETCH from %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Failed to FETCH from %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_JPEG
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)
 
@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to setup %TMP_SRC%!
@echo %HAD_ERROR%: Failed to setup %TMP_SRC%! >> %ERRLOG%
@goto DN_JPEG
)

CD %TMP_SRC%

@IF NOT EXIST jconfig.h (
@echo Doing 'nmake -f makefile.vc setup-v10'
nmake -f makefile.vc setup-v10
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit makefile.vc %TMP_SRC%
@echo %HAD_ERROR%: Error exit makefile.vc %TMP_SRC% >> %ERRLOG%
)
)
@REM sed -i "s/Win32/x64/g" jpeg.sln
@REM sed -i "s/Win32/x64/g" jpeg.vcxproj
perl -f %PERL_FIL% jpeg.sln
perl -f %PERL_FIL% jpeg.vcxproj
ECHO Doing 'msbuild jpeg.sln /t:Build /p:Configuration=Release;Platform=x64' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing 'msbuild jpeg.sln /t:Build /p:Configuration=Release;Platform=x64' to %LOGFIL%
)
msbuild jpeg.sln /t:Build /p:Configuration=Release;Platform=x64 %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit msbuild source %TMP_SRC%
@echo %HAD_ERROR%: Error exit msbuild source %TMP_SRC% >> %ERRLOG%
)

@echo Installing the jpeg built components...
xcopy %WORKSPACE%\libjpeg-source\x64\Release\jpeg.lib %WORKSPACE%\%TMP3RD%\lib /y /s /q
xcopy %WORKSPACE%\libjpeg-source\jconfig.h %WORKSPACE%\%TMP3RD%\include /y /s /q
xcopy %WORKSPACE%\libjpeg-source\jerror.h %WORKSPACE%\%TMP3RD%\include /y /s /q
xcopy %WORKSPACE%\libjpeg-source\jmorecfg.h %WORKSPACE%\%TMP3RD%\include /y /s /q
xcopy %WORKSPACE%\libjpeg-source\jpeglib.h %WORKSPACE%\%TMP3RD%\include /y /s /q

:DN_JPEG
cd %WORKSPACE%

:DO_CURL
 
@echo %0: ############################# Download ^& compile LIBCURL %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBCURL to %LOGFIL%
)
@set TMP_URL=http://curl.haxx.se/download/curl-7.35.0.zip
@set TMP_ZIP=libcurl.zip
@set TMP_SRC=libcurl-source
@set TMP_DIR=curl-7.35.0
@set TMP_BLD=libcurl-build

@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error failed download from %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Error failed download from %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_CURL
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error failed set up of %TMP_SRC%
@echo %HAD_ERROR%: Error failed set up of %TMP_SRC% >> %ERRLOG%
@goto DN_CURL
) 

cd %WORKSPACE%

if NOT EXIST %TMP_BLD%\nul (
MD %TMP_BLD%
)

CD %TMP_BLD%
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libcurl-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%"' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libcurl-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%"' to %LOGFIL%
)
cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libcurl-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%" %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC%
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC% >> %ERRLOG%
)
ECHO Doing: 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake --build . --config Release --target INSTALL' to %LOGFIL%
)
cmake --build . --config Release --target INSTALL %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)

cd %WORKSPACE%
 
xcopy %WORKSPACE%\libcurl-build\build\include\* %WORKSPACE%\%TMP3RD%\include /y /s /q
xcopy %WORKSPACE%\libcurl-build\build\lib\libcurl_imp.lib %WORKSPACE%\%TMP3RD%\lib /y /q
xcopy %WORKSPACE%\libcurl-build\build\lib\libcurl.dll %WORKSPACE%\%TMP3RD%\bin /y /q
xcopy %WORKSPACE%\libcurl-build\build\bin\curl.exe %WORKSPACE%\%TMP3RD%\bin /y /q

:DN_CURL
cd %WORKSPACE%
:DO_GDAL 
@if %ADD_GDAL% EQU 0 goto DN_GDAL
 
@echo %0: ############################# Download ^& compile GDAL %CD% %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile GDAL %CD% to %LOGFIL%
)
@REM set TMP_URL=https://svn.osgeo.org/gdal/trunk/gdal
@REM This SVN source FAILED to link with CGAL
@set TMP_SRC=libgdal-source
@set TMP_URL=http://download.osgeo.org/gdal/1.11.0/gdal1110.zip
@set TMP_ZIP=libgdal.zip
@set TMP_DIR=gdal-1.11.0

@if NOT EXIST %TMP_ZIP% ( 
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_GDAL
)

CD %TMP_SRC%
ECHO Doing: 'nmake -f makefile.vc MSVC_VER=1600 GDAL_HOME=%WORKSPACE%/libgdal-source BINDIR=%WORKSPACE%\%TMP3RD%\bin LIBDIR=%WORKSPACE%\%TMP3RD%\lib INCDIR=%WORKSPACE%\%TMP3RD%\include WIN64=YES' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'nmake -f makefile.vc MSVC_VER=1600 GDAL_HOME=%WORKSPACE%/libgdal-source BINDIR=%WORKSPACE%\%TMP3RD%\bin LIBDIR=%WORKSPACE%\%TMP3RD%\lib INCDIR=%WORKSPACE%\%TMP3RD%\include WIN64=YES' to %LOGFIL%
)
nmake -f makefile.vc MSVC_VER=1600 GDAL_HOME=%WORKSPACE%/libgdal-source BINDIR=%WORKSPACE%\%TMP3RD%\bin LIBDIR=%WORKSPACE%\%TMP3RD%\lib INCDIR=%WORKSPACE%\%TMP3RD%\include WIN64=YES %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit nmake building source %TMP_SRC% in %CD%
@echo %HAD_ERROR%: Error exit nmake building source %TMP_SRC% in %CD% >> %ERRLOG%
)

cd %WORKSPACE%
 
xcopy %WORKSPACE%\libgdal-source\gcore\gdal.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\gcore\gdal_frmts.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\gcore\gdal_proxy.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\gcore\gdal_priv.h %WORKSPACE%\%TMP3RD%\include\ /y /f 
xcopy %WORKSPACE%\libgdal-source\gcore\gdal_version.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\alg\gdal_alg.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\alg\gdalwarper.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\frmts\vrt\gdal_vrt.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\ogr\ogr*.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\port\cpl*.h %WORKSPACE%\%TMP3RD%\include\ /y /f
xcopy %WORKSPACE%\libgdal-source\gdal_i.lib %WORKSPACE%\%TMP3RD%\lib\ /y /f
xcopy %WORKSPACE%\libgdal-source\gdal.lib %WORKSPACE%\%TMP3RD%\lib\ /y /f
xcopy %WORKSPACE%\libgdal-source\gdal*.dll %WORKSPACE%\%TMP3RD%\bin\ /y /f

:DN_GDAL
cd %WORKSPACE%
@REM TEST EXIT
@REM GOTO END
:DO_FLTK

@echo %0: ############################# Download ^& compile LIBFLTK %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBFLTK to %LOGFIL%
)
@set TMP_URL=http://fltk.org/pub/fltk/1.3.2/fltk-1.3.2-source.tar.gz
@set TMP_ZIP=libfltk.tar.gz
@set TMP_TAR=libfltk.tar
@set TMP_SRC=libfltk-source
@set TMP_BLD=libfltk-build
@set TMP_DIR=fltk-1.3.2

@if NOT EXIST %TMP_TAR% (
@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)

@if NOT EXIST %TMP_TAR% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to fetch %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Failed to fetch %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_FLTK
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_TAR%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_FLTK
)

cd %WORKSPACE%

@if NOT EXIST %TMP_BLD%\nul (
md %TMP_BLD%
)

cd %TMP_BLD%
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libfltk-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%"' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libfltk-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%"' to %LOGFIL%
)
cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libfltk-build\build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%\%TMP3RD%" %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC%
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC% >> %ERRLOG%
)

ECHO Doing: 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake --build . --config Release --target INSTALL' to %LOGFIL%
)
cmake --build . --config Release --target INSTALL %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)

cd %WORKSPACE%
 
xcopy %WORKSPACE%\libfltk-build\build\include\* %WORKSPACE%\%TMP3RD%\include /y /s /q
xcopy %WORKSPACE%\libfltk-build\build\bin\* %WORKSPACE%\%TMP3RD%\bin /y /s /q
xcopy %WORKSPACE%\libfltk-build\build\lib\fltk*.lib %WORKSPACE%\%TMP3RD%\lib /y /s /q

:DN_FLTK
cd %WORKSPACE%

:DO_BOOST
 
@echo %0: ############################# Download ^& compile LIBBOOST %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBBOOST to %LOGFIL%
)
ECHO However this is ONLY obtaining the simpits boost and binaries %BLDLOG%
ECHO while download_and_compile obtains the SVN source and does a compile %BLDLOG%
ECHO So this is presently SKIPPED %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO However this is ONLY obtaining the simpits boost and binaries
ECHO while download_and_compile obtains the SVN source and does a compile
ECHO So this is presently SKIPPED
)
@GOTO DN_BOOST

@REM set TMP_URL=http://flightgear.simpits.org:8080/job/Boost-Win64/lastSuccessfulBuild/artifact/*zip*/archive.zip
@set TMP_URL=http://flightgear.simpits.org:8080/job/Boost-Win64/lastSuccessfulBuild/artifact/*zip*/Boost.zip
@set TMP_ZIP=libboost.zip
@set TMP_SRC=Boost

@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)

@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Download from %TMP_URL% to %TMP_ZIP% FAILED! >> %ERRLOG%
@echo %HAD_ERROR%: Download from %TMP_URL% to %TMP_ZIP% FAILED!
@goto DN_BOOST
)

@if NOT EXIST Boost\nul (
@if NOT EXIST archive\Boost\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
MOVE archive\Boost .
RMDIR archive
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%!
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%! >> %ERRLOG%
@goto DN_BOOST
)

CD %TMP_SRC%

@if NOT EXIST lib\nul (
REN lib64 lib
)

@if NOT EXIST include\boost-1_55\nul (
MD include\boost-1_55
)

@if EXIST boost (
MOVE boost include\boost-1_55
)

:DN_BOOST 

cd %WORKSPACE%

:DO_CGAL

@echo %0: ############################# Download ^& compile CGAL %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile CGAL to %LOGFIL%
)
@REM set TMP_URL=https://gforge.inria.fr/frs/download.php/32996/CGAL-4.3.zip
@set TMP_URL=https://gforge.inria.fr/frs/download.php/file/33527/CGAL-4.4.zip
@set TMP_ZIP=libcgal.zip
@set TMP_SRC=libcgal-source
@set TMP_BLD=libcgal-build
@set TMP_DIR=CGAL-4.4
@set TMP_PRE=%WORKSPACE%\cgal-source\auxiliary\gmp;%WORKSPACE%\install\Boost

@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed download from %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Failed download from %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_CGAL
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

xcopy "%CGAL_PATH%"\auxiliary\gmp\include\* %WORKSPACE%\libcgal-source\auxiliary\gmp\include /s /y /i
xcopy "%CGAL_PATH%"\auxiliary\gmp\lib64\* %WORKSPACE%\libcgal-source\auxiliary\gmp\lib /s /y /i
xcopy "%CGAL_PATH%"\auxiliary\gmp\lib\* %WORKSPACE%\libcgal-source\auxiliary\gmp\lib /s /y /i
 
CD %WORKSPACE%

@if NOT EXIST %TMP_BLD%\nul (
MD %TMP_BLD%
)

CD %TMP_BLD%
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_PREFIX_PATH="%TMP_PRE%" -DCGAL_Boost_USE_STATIC_LIBS:BOOL=ON -DZLIB_LIBRARY="%WORKSPACE%\%TMP3RD%\lib\zlib.lib" -DZLIB_INCLUDE_DIR="%WORKSPACE%\%TMP3RD%\include" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libcgal-build\build" %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_PREFIX_PATH="%TMP_PRE%" -DCGAL_Boost_USE_STATIC_LIBS:BOOL=ON -DZLIB_LIBRARY="%WORKSPACE%\%TMP3RD%\lib\zlib.lib" -DZLIB_INCLUDE_DIR="%WORKSPACE%\%TMP3RD%\include" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libcgal-build\build" to %LOGFIL%
)
cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_PREFIX_PATH="%TMP_PRE%" -DCGAL_Boost_USE_STATIC_LIBS:BOOL=ON -DZLIB_LIBRARY="%WORKSPACE%\%TMP3RD%\lib\zlib.lib" -DZLIB_INCLUDE_DIR="%WORKSPACE%\%TMP3RD%\include" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libcgal-build\build" %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Config/Gen FAILED %TMP_SRC%
@echo %HAD_ERROR%: Config/Gen FAILED %TMP_SRC% >> %ERRLOG%
@REM goto DN_CGAL
)
ECHO Doing: 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake --build . --config Release --target INSTALL' to %LOGFIL%
)
cmake --build . --config Release --target INSTALL %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Build FAILED %TMP_SRC%
@echo %HAD_ERROR%: Build FAILED %TMP_SRC% >> %ERRLOG%
@REM goto DN_CGAL
)
 
cd %WORKSPACE%


@echo Doing: xcopy %WORKSPACE%\libcgal-build\build\include\* %WORKSPACE%\%TMP3RD%\include /y /s /q
xcopy %WORKSPACE%\libcgal-build\build\include\* %WORKSPACE%\%TMP3RD%\include /y /s /q
@echo Doing: xcopy %WORKSPACE%\libcgal-build\build\bin\*.dll %WORKSPACE%\%TMP3RD%\bin /y /s /q
xcopy %WORKSPACE%\libcgal-build\build\bin\*.dll %WORKSPACE%\%TMP3RD%\bin /y /s /q
@echo Doing: xcopy %WORKSPACE%\libcgal-build\build\lib\*.lib %WORKSPACE%\%TMP3RD%\lib /y /s /q
xcopy %WORKSPACE%\libcgal-build\build\lib\*.lib %WORKSPACE%\%TMP3RD%\lib /y /s /q
@echo Doing: xcopy %WORKSPACE%\libcgal-source\auxiliary\gmp\lib\*.dll %WORKSPACE%\%TMP3RD%\bin /s /y /q
xcopy %WORKSPACE%\libcgal-source\auxiliary\gmp\lib\*.dll %WORKSPACE%\%TMP3RD%\bin /s /y /q
@echo Doing: xcopy %WORKSPACE%\libcgal-source\auxiliary\gmp\lib\*.lib %WORKSPACE%\%TMP3RD%\lib /s /y /q
xcopy %WORKSPACE%\libcgal-source\auxiliary\gmp\lib\*.lib %WORKSPACE%\%TMP3RD%\lib /s /y /q

 
:DN_CGAL
cd %WORKSPACE%
REM TEST EXIT
REM GOTO END

:DO_FREETYPE
 
@echo %0: ############################# Download ^& compile FREETYPE %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile FREETYPE to %LOGFIL%
)

@set TMP_URL=http://sourceforge.net/projects/freetype/files/freetype2/2.5.3/ft253.zip/download
@set TMP_ZIP=freetype.zip
@set TMP_SRC=freetype-source
@set TMP_BLD=freetype-build
@set TMP_DIR=freetype-2.5.3

@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to download %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Failed to download %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_FREETYPE
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_FREETYPE
)

CD %WORKSPACE%

@if NOT EXIST %TMP_BLD%\nul (
MD %TMP_BLD%
)

CD %TMP_BLD%
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%/freetype-build/build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%/%TMP3RD%"' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%/freetype-build/build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%/%TMP3RD%"' to %LOGFIL%
) 
cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%/freetype-build/build" -DCMAKE_PREFIX_PATH:PATH="%WORKSPACE%/%TMP3RD%"
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC%
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC% >> %ERRLOG%
)

ECHO Doing: 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
ECHO Doing: 'cmake --build . --config Release --target INSTALL' to %LOGFIL%
)
cmake --build . --config Release --target INSTALL %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)

CD %WORKSPACE%
 
xcopy %WORKSPACE%\freetype-build\build\* %WORKSPACE%\%TMP3RD% /y /s /q

:DN_FREETYPE

cd %WORKSPACE%

:DO_PROJ
@echo %0: ############################# Download ^& compile LIBPROJ %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBPROJ to %LOGFIL%
)
@set TMP_URL=http://download.osgeo.org/proj/proj-4.8.0.zip
@set TMP_ZIP=libproj.zip
@set TMP_SRC=libproj-source
@set TMP_DIR=proj-4.8.0

@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
) 
@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed download %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Failed download %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_PROJ
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_DIR%\nul (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to setup %TMP_SRC%
@echo %HAD_ERROR%: Failed to setup %TMP_SRC% >> %ERRLOG%
@goto DN_PROJ
)

CD %TMP_SRC%
@echo Doing:  'nmake -f makefile.vc' %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo Doing:  'nmake -f makefile.vc' to %LOGFIL%
)
nmake -f makefile.vc %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit nmake makefile.vc %TMP_SRC%
@echo %HAD_ERROR%: Error exit nmake maekfile.vc %TMP_SRC% >> %ERRLOG%
)

CD %WORKSPACE%
 
xcopy %WORKSPACE%\libproj-source\src\*.lib %WORKSPACE%\%TMP3RD%\lib /s /y /q
xcopy %WORKSPACE%\libproj-source\src\*.dll %WORKSPACE%\%TMP3RD%\bin /s /y /q
xcopy %WORKSPACE%\libproj-source\src\proj_api.h %WORKSPACE%\%TMP3RD%\include /s /y /q

:DN_PROJ
cd %WORKSPACE%
 
:DO_GEOS 
  
@echo %0: ############################# Download ^& compile LIBGEOS %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBGEOS to %LOGFIL%
)
@set TMP_URL=http://download.osgeo.org/geos/geos-3.4.2.tar.bz2
@set TMP_ZIP=libgeos.tar.bz2
@set TMP_TAR=libgeos.tar
@set TMP_SRC=libgeos-source
@set TMP_BLD=libgeos-build
@set TMP_DIR=geos-3.4.2

@if NOT EXIST %TMP_TAR% (
@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)

@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1 
@echo %HAD_ERROR%: Failed download from %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Failed download from %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_GEOS
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_TAR% (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL %UZ_EXE% %UZ_OPT% %TMP_TAR%
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_GEOS
)

cd %WORKSPACE%
@if NOT EXIST %TMP_BLD%\nul (
md %TMP_BLD%
)

cd %TMP_BLD%
@echo Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libgeos-build\build"' %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libgeos-build\build"' to %LOGFIL%
)
cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libgeos-build\build" %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC%
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC% >> %ERRLOG%
)

@ECHO Doing: 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
@ECHO Doing: 'cmake --build . --config Release --target INSTALL' to %LOGFIL%
)
cmake --build . --config Release --target INSTALL %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)

cd %WORKSPACE%
 
xcopy %WORKSPACE%\libgeos-build\build\bin\geos_c.dll %WORKSPACE%\%TMP3RD%\bin /s /y /q
xcopy %WORKSPACE%\libgeos-build\build\lib\geos_c.lib %WORKSPACE%\%TMP3RD%\lib /s /y /q
xcopy %WORKSPACE%\libgeos-build\build\include\geos_c.h %WORKSPACE%\%TMP3RD%\include /s /y /q

:DN_GEOS
cd %WORKSPACE%

:DO_EXPAT
@echo %0: ############################# Download ^& compile LIBEXPAT %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo %0: ############################# Download ^& compile LIBEXPAT to %LOGFIL%
)
@set TMP_URL=http://sourceforge.net/projects/expat/files/expat/2.1.0/expat-2.1.0.tar.gz/download
@set TMP_TAR=libexpat.tar
@set TMP_ZIP=libexpat.tar.gz
@set TMP_SRC=libexpat-source
@set TMP_BLD=libexpat-build
@set TMP_DIR=expat-2.1.0

@if NOT EXIST %TMP_TAR% (
@if NOT EXIST %TMP_ZIP% (
CALL %GET_EXE% %TMP_URL% %GET_OPT% %TMP_ZIP%
)
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)

@if NOT EXIST %TMP_ZIP% (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed download from %TMP_URL% to %TMP_ZIP%
@echo %HAD_ERROR%: Failed download from %TMP_URL% to %TMP_ZIP% >> %ERRLOG%
@goto DN_EXPAT
)

@if NOT EXIST %TMP_SRC%\nul (
@if NOT EXIST %TMP_TAR% (
CALL %UZ_EXE% %UZ_OPT% %TMP_ZIP%
)
CALL %UZ_EXE% %UZ_OPT% %TMP_TAR%
CALL :SLEEP1
REN %TMP_DIR% %TMP_SRC%
)

@if NOT EXIST %TMP_SRC%\nul (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Failed to set up %TMP_SRC%
@echo %HAD_ERROR%: Failed to set up %TMP_SRC% >> %ERRLOG%
@goto DN_EXPAT
)
 
cd %WORKSPACE%

@if NOT EXIST %TMP_BLD%\nul (
md %TMP_BLD%
)

cd %TMP_BLD%
@echo Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libexpat-build\build"' %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo Doing: 'cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libexpat-build\build"' to %LOGFIL%
)
cmake ..\%TMP_SRC% -G "Visual Studio 10 Win64" -DCMAKE_INSTALL_PREFIX:PATH="%WORKSPACE%\libexpat-build\build" %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC%
@echo %HAD_ERROR%: Error exit cmake conf/gen %TMP_SRC% >> %ERRLOG%
)

@echo Doing: 'cmake --build . --config Release --target INSTALL' %BLDLOG%
IF %HAVELOG% EQU 1 (
@echo Doing: 'cmake --build . --config Release --target INSTALL' to %LOGFIL%
)
cmake --build . --config Release --target INSTALL %BLDLOG%
@if ERRORLEVEL 1 (
@set /A HAD_ERROR+=1
@echo %HAD_ERROR%: Error exit building source %TMP_SRC%
@echo %HAD_ERROR%: Error exit building source %TMP_SRC% >> %ERRLOG%
)
 
xcopy %WORKSPACE%\libexpat-build\build\bin\expat.dll %WORKSPACE%\%TMP3RD%\bin /s /y /q
xcopy %WORKSPACE%\libexpat-build\build\lib\expat.lib %WORKSPACE%\%TMP3RD%\lib /s /y /q
xcopy %WORKSPACE%\libexpat-build\build\include\* %WORKSPACE%\%TMP3RD%\include /s /y /q
 
:DN_EXPAT
 
:END
@if NOT %HAD_ERROR% EQU 0 goto ISERR
@echo Appears a fully successful build...
@endlocal
@exit /b 0

:ISERR
@echo.
type %ERRLOG%
@echo.
@echo Note: Had %HAD_ERROR% ERRORS during the build...
@echo Perhaps above %ERRLOG% output may have details...
@endlocal
@exit /b %HAD_ERROR% 

:SLEEP1
@timeout -t 1 >nul 2>&1
@goto :EOF

REM eof
