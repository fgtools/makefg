README.md - 20150616

This is a script to build FlightGear in Ubuntu. It tries to handle ALL 
the header and library dependencies...

It has quite a number of command line features, all of which can be seen by -
```
 $ makefg -?
```
 
It is targeted to my Ubuntu 12.04 LTS, and later a full re-intall of 14.04.1 LTS (trusty)
x86_64 (64-bit) system, but should work in most Debian linux systems...

One very important switch when first run in a new folder, and that is **NO_TOOL_UPD**!
If your system has already had lots of tool sets, and common libraries installed,
then it should NOT be necessary to 'update' such tools.

The problem is that the list changes with every version of linux, and the list 
built into the script gets out of date very easily. If a build of a component 
fails due to some missing header, or library, then you can install the ...-dev
version using -
```
 $ sudo apt-get install <component>
``` 
or use the Synaptic Package Manager to search, and install the required item...

The latest version is always 'makefg', but older version are stored with a 
version appended in the 'linux/versions' folder.

After the first FULL build, 'makefg' can be run again, and again to continue with 
updates of any component... skipping or including various stages..

Typically only an update of simgear, flightgear and fgdata is all that is required. 

The fgdata base package is usually a heavy, long task, so that is best done 'manually' -
``` 
 $ cd fgdata 
 $ git pull
``` 
to get that over with...

So a typical update command could be -
```
 $ makefg NOOSG NOFGRUN FGUPD SGUPD FGNODATA NOPAUSE
``` 
and it will all be done while you sip your coffee... another little script, updfgrt.sh
is added to do the above...

I would welcome collabitrators to further extend and support this script.

Regards,  
Geoff. 20150616

; eof
