#!/bin/sh
#< updfgrt.sh - 20140811 - 20120328 - to fully update FG runtime
# 20130130 - Allow other options to be added
# 20120501 - Update fgdata separately, and ADD clean
BN=`basename $0`
OPTS="NOOSG NOFGRUN FGUPD SGUPD FGNODATA NOPAUSE"
MSG=""
ADDED=""
for arg in $@; do
    if [ "$arg" = "CLEAN" ]; then
        OPTS="FGCLEAN SGCLEAN $OPTS"
        MSG="$MSG Added CLEAN to script"
    else
        OPTS="$OPTS $arg"
        ADDED="$ADDED $arg"
        #echo "$BN: ERROR: Unknown option '$arg'!"
        #echo "$BN: Only option is 'CLEAN'"
        #exit 1
    fi
done
if [ -z "$MSG" ]; then
    MSG="Did NOT get CLEAN option, so no clean"
fi
if [ -z "$ADDED" ]; then
    echo "$BN: NO extra options added"
else
    echo "$BN: Added new option $ADDED"
fi
echo "$BN: $MSG"
echo "$BN: Doing makefg $OPTS"
makefg $OPTS
echo "$BN: fgdata base package should be updated separately, if NOT done..."

# eof

